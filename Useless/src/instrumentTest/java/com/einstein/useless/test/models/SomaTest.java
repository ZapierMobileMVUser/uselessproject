package com.einstein.useless.test.models;

import android.test.InstrumentationTestCase;

import com.einstein.useless.models.Soma;

/**
 * Created by rafaelduarte on 6/26/13.
 */
public class SomaTest extends InstrumentationTestCase {
    public void testSomaPositivos() throws Exception {
        assertEquals(5, Soma.somaInteiros(3, 2));
    }

    public void testSomaPositivoComNegativo() throws Exception {
        assertEquals(1, Soma.somaInteiros(3, -2));
    }
}
